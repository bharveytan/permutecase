import permuteCase from './permuteCase';

describe('permuteCase', () => {
    describe.each`
    input       | expectedPermutationArray 
    ${'aaa'}    | ${["aaa", "aaA", "aAa", "aAA", "Aaa", "AaA", "AAa", "AAA"]}
    ${'aB3m'}   | ${["ab3m", "Ab3m", "aB3m", "AB3m", "ab3M", "Ab3M", "aB3M", "AB3M"]}
    ${''}       | ${['']}
    ${'000'}    | ${['000']}
    `('when input is $input', ({input, expectedPermutationArray}) => {
        it('returns the expected case permutation array, order ignored', () => {
           expect(permuteCase(input).sort()).toEqual(expectedPermutationArray.sort());
        });
    });
});
