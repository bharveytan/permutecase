export default (sample) => {
    const loweredSample = sample.toLowerCase();
    let result = [];

    const findOnes = (bits) => {
        let arrPositions = [];


        for (let i = 0; i < bits.length; i++) {
            if (bits[i] === '1') {
                arrPositions.push(i);
            }
        }

        return arrPositions;
    };

    const maxPermutations = 2 ** loweredSample.length;

    for (let i = 0; i < maxPermutations; i++) {
        let hasDigit = false;
        let stringToInsert = loweredSample;
        const binaryOfIndex = i.toString(2).padStart(loweredSample.length, '0');
        let onePositions = findOnes(binaryOfIndex);

        onePositions.forEach( (pos) => {
            if (!(/^[a-zA-Z]/.test(stringToInsert.charAt(pos))) ){
                hasDigit = true;
            }
            stringToInsert = stringToInsert.slice(0, pos) + stringToInsert.charAt(pos).toUpperCase() + stringToInsert.slice(pos + 1);
        });
        !hasDigit && result.push(stringToInsert);
    }

    return result;

}
